resource "libvirt_pool" "ubuntu" {
 name = "ubuntu-pool"
 type = "dir"
 path = "/libvirt_images/ubuntu-pool/"
}

resource "libvirt_network" "kube_network" {
  autostart = true
  name      = "k8snet"
  mode      = "nat"
  #domain    = "k8s.local"
  # IPv6 ULA address: https://tools.ietf.org/html/rfc4193
  addresses = ["172.16.0.0/24", "fd4a:fc40:8cfb::/64"]

  dhcp {
    enabled = false
  }

  bridge    = "k8snet-br"
  dns {
    enabled    = true
    local_only = true
  }
}

resource "libvirt_volume" "image-qcow2" {
  name = "ubuntu-amd64-${count.index + 1}.qcow2"
  count = "3"
  pool = libvirt_pool.ubuntu.name
  source ="${path.module}/downloads/bionic-server-cloudimg-amd64.img"
  #source = "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64-disk-kvm.img"
  format = "qcow2"
}

resource "libvirt_domain" "test-domain" {
  # name should be unique!
  name = "test-vm-ubuntu-${count.index + 1}"
  memory = "2048"
  vcpu = 2
  count = "3"
  # add the cloud init disk to share user data
  cloudinit = libvirt_cloudinit_disk.commoninit.id
  # set to default libvirt network
  network_interface {
    network_name   = libvirt_network.kube_network.name
    hostname       = "test-vm-ubuntu-${count.index + 1}"
    addresses      = ["172.16.0.${count.index + 11}" ]
    wait_for_lease = false
  }
  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }
  disk {
    volume_id = libvirt_volume.image-qcow2[count.index].id
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}
